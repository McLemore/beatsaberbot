import org.kitteh.irc.client.library.Client;
import org.kitteh.irc.client.library.feature.twitch.TwitchDelaySender;
import org.kitteh.irc.client.library.feature.twitch.TwitchListener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class BeatBotMain {

    private static final String BOTNAME = "BeatSaberBot";
    private  static String OAUTH = "";
    static String CHANNEL = "";
    static boolean MODONLY = false;
    static boolean SUBONLY = false;
    static boolean OVERRIDE_SUB_REQ = false;
    static boolean DEBUG = false;
    static boolean CONT_QUEUE = false;
    static int VIEWER_REQ = 10;


    static Client client;
    private static SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

    public static void main(String[] args) throws Exception {
        Properties prop = new Properties();
        InputStream input = null;

        try{
            input = new FileInputStream("botsaber.properties");
            prop.load(input);

            if(!prop.getProperty("oauth_token").isEmpty()){
                OAUTH = prop.getProperty("oauth_token");
            }

            if(!prop.getProperty("twitch_channel").isEmpty()){
                CHANNEL = prop.getProperty("twitch_channel");
            }

            if(prop.getProperty("moderator_only").equalsIgnoreCase("yes")){
                MODONLY = true;
            }

            if(prop.getProperty("subscriber_only").equalsIgnoreCase("yes")){
                SUBONLY = true;
            }

            if(prop.getProperty("debug_output").equalsIgnoreCase("yes")){
                DEBUG = true;
            }

            if(prop.getProperty("continue_queue").equalsIgnoreCase("yes")){
                CONT_QUEUE = true;
            }

            if(!prop.getProperty("viewer_request_count_allowed").isEmpty()){
                try{
                    VIEWER_REQ = Integer.parseInt(prop.getProperty("viewer_request_count_allowed"));
                } catch (NumberFormatException e){
                    VIEWER_REQ = 10;
                    throw new IOException("Viewer Request Count should be a number value, defaulted to 10");
                }
            }

            if(prop.getProperty("override_request_limit_subscriber").equalsIgnoreCase("yes")){
                OVERRIDE_SUB_REQ = true;
            }

        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(input != null){
                try {
                    input.close();
                } catch (IOException ex){
                    ex.printStackTrace();
                }
            }
        }

        if(OAUTH.isEmpty()){
            throw new Exception("OAuth Token is invalid");
        }else if(CHANNEL.isEmpty()){
            throw new Exception("Channel name is invalid");
        }else {

            if(DEBUG){
                System.out.println("Debug Option Enabled");
                System.out.println("Connected to " + CHANNEL + " using token: " + OAUTH);
                System.out.println("Moderator Mode: " + MODONLY);
                System.out.println("Subscriber Mode: " + SUBONLY);
                System.out.println("Viewer Request Limit: " + VIEWER_REQ);
                System.out.println("Continue Queue'd Requests: " + CONT_QUEUE);
                System.out.println();
            }

            client = Client.builder()
                    .serverHost("irc.chat.twitch.tv").serverPort(443)
                    .serverPassword(OAUTH)
                    .nick(BOTNAME)
                    .messageSendingQueueSupplier(TwitchDelaySender.getSupplier(false))
                    .build();
            client.getEventManager().registerEventListener(new TwitchListener(client));
            client.getEventManager().registerEventListener(new BeatQueueBot.Listener());
            client.connect();

            client.addChannel("#" + CHANNEL);

            if(prop.getProperty("output_irc_output").equalsIgnoreCase("yes")){
                client.setInputListener(line -> System.out.println(sdf.format(new Date()) + ' ' + "[I] " + line));
                client.setOutputListener(line -> System.out.println(sdf.format(new Date()) + ' ' + "[O] " + line));
            }

        }
    }
}