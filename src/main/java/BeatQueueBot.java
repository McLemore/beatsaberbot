import com.sun.corba.se.spi.orb.StringPair;
import net.engio.mbassy.listener.Handler;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.kitteh.irc.client.library.event.client.ClientReceiveCommandEvent;
import org.kitteh.irc.client.library.feature.filter.CommandFilter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeatQueueBot {

   // private List<String> queueList = new ArrayList<>();
    private final List<String> banList = new ArrayList<>();
    private final ArrayList<StringPair> queueList = new ArrayList<>();

    private String blacklistFilePath;
    private String playlistFilePath;
    private static boolean isModerator = false;
    private static boolean isSubscriber = false;
    private static boolean isBroadcaster = false;
    private static boolean isAcceptingRequests = true;

    public static class Listener {

        final BeatQueueBot bqb = new BeatQueueBot();

        @CommandFilter("PRIVMSG")
        @Handler
        public void privmsg(ClientReceiveCommandEvent event) {
            String user = null;
            if(event.getServerMessage().getTag("display-name").isPresent()){
                if(event.getServerMessage().getTag("display-name").get().getValue().isPresent()) {
                    user = event.getServerMessage().getTag("display-name").get().getValue().get();
                }
            }

            String[] message = event.getOriginalMessage().split(":");

            if(event.getMessageTags().get(0).getValue().isPresent()) {
                isBroadcaster = event.getMessageTags().get(0).getValue().get().contains("broadcaster");
            } else
                isBroadcaster = false;

            if(event.getServerMessage().getTag("mod").isPresent()) {
                isModerator = event.getServerMessage().getTag("mod").get().getValue().toString().equals("Optional[1]");
            } else
                isModerator = false;

            if(event.getServerMessage().getTag("subscriber").isPresent()) {
                isSubscriber = event.getServerMessage().getTag("subscriber").get().getValue().toString().equals("Optional[1]");
            } else
                isSubscriber = false;

            if(BeatBotMain.DEBUG) {
                System.out.println(event.getOriginalMessage());
                System.out.println("Is Moderator: " + isModerator);
                System.out.println("Is Subscriber: " + isSubscriber);
                System.out.println("Is Broadcaster: " + isBroadcaster);
                System.out.println();
            }

            // Sends only the message portion of the chat
            bqb.runCommands(message[2], user);
        }
    }

    private BeatQueueBot(){

        try {
            File f = new File("song_blacklist.txt");
            blacklistFilePath = f.getAbsolutePath();
            if (f.createNewFile()) {
                System.out.println("Blacklist File Created");
            } else {
                assignBlacklistedSongs(f.getAbsolutePath());
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            File f = new File("playlist.json");
            playlistFilePath = f.getAbsolutePath();
            if (f.createNewFile()) {
                createJSON();
            } else {
                if(!BeatBotMain.CONT_QUEUE){
                    createJSON();
                } else
                    processExistingJSON();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //This will return the response from the command
    private void runCommands(String command, String requestUser) {
        if ((command.startsWith("!next") || command.startsWith("!clearall") || command.startsWith("!remove") || command.startsWith("!block") || command.startsWith("!close") || command.startsWith(("!open"))) && (isModerator || isBroadcaster)) {
            if (command.startsWith("!next") ) {
                if (queueList.size() >= 1) {
                    String remSong = queueList.get(0).getFirst();
                    queueList.remove(0);
                    removeValueFromJSON(false, null);
                    if (queueList.size() != 0) {
                        sendMessage("Removed \"" + remSong + "\" from the queue, next song is \"" + queueList.get(0).getFirst() + "\" requested by " + queueList.get(0).getSecond());
                    } else
                        sendMessage("Queue is now empty");
                } else
                    sendMessage("BeatSaber queue was empty.");
            } else if (command.startsWith("!clearall") ) {
                if (queueList.size() != 0) {
                    queueList.clear();
                    sendMessage("Removed all songs from the BeatSaber queue");
                } else
                    sendMessage("BeatSaber queue was empty");
            } else if (command.startsWith("!remove") ) {
                String[] parsedCommand = command.split("[\\s+]", 2);
                if (parsedCommand.length > 1) {
                    String[] beatSaverValues = getSongFromBeatSaver(parsedCommand[1]);
                    if(beatSaverValues != null) {
                        for(int i = 0; i < queueList.size(); i++){
                            if (queueList.get(i).getFirst().contains(beatSaverValues[0])) {
                                queueList.remove(i);
                            }
                        }
                        removeValueFromJSON(true, beatSaverValues[0]);
                        sendMessage("Removed \"" + beatSaverValues[0] + "\" from the queue");
                    }
                }
            } else if (command.startsWith("!block") ) {
                String[] parsedCommand = command.split("[\\s+]");
                if (parsedCommand.length > 1) {
                    if (banList.contains(parsedCommand[1])) {
                        sendMessage("Song is already on Banlist");
                    } else {
                        addSongToBlackList(parsedCommand[1]);
                    }
                } else
                    sendMessage("Missing song ID");

            } else if (command.startsWith("!close")) {
                if(isAcceptingRequests) {
                    isAcceptingRequests = false;
                    sendMessage("The queue has been closed!");
                } else {
                    sendMessage("The queue was already closed.");
                }
            } else if (command.startsWith("!open")) {
                if(!isAcceptingRequests) {
                    isAcceptingRequests = true;
                    sendMessage("The queue has been opened!");
                } else {
                    sendMessage("The queue is already open.");
                }
            }
        } else if(BeatBotMain.MODONLY){
            if(isModerator || isBroadcaster){
                basicCommands(command, requestUser);
            }
        } else if(BeatBotMain.SUBONLY ) {
            if(isModerator || isBroadcaster || isSubscriber) {
                basicCommands(command, requestUser);
            }
        } else if(!BeatBotMain.MODONLY && !BeatBotMain.SUBONLY ) {
            basicCommands(command, requestUser);
        }
    }

    private void basicCommands(String command, String requestUser){
        if (command.startsWith("!queue")) {
            if (queueList.size() != 0) {
                StringBuilder curr = new StringBuilder("Current song list: ");
                for (int i = 0; i < queueList.size(); i++) {
                    if (i < queueList.size() - 1) {
                        curr.append(queueList.get(i).getFirst()).append(", ");
                    } else
                        curr.append(queueList.get(i).getFirst());
                }
                sendMessage(curr.toString());
            } else
                sendMessage("No songs in the queue");
        } else if (command.startsWith("!add")) {
            if(!isAcceptingRequests) {
                sendMessage("The queue is currently closed.");
                return;
            }

            String[] parsedCommand = command.split("[\\s+]");
            if (parsedCommand.length > 1) {
                // Check for song against Blacklist, else proceed to get Song information
                if (banList.contains(parsedCommand[1])) {
                    sendMessage("Song is currently Blacklisted");
                } else
                    addSongToQueue(parsedCommand[1], requestUser);
            } else
                sendMessage("Missing song ID");
        } else if (command.startsWith("!blist")) {
            String bannedSongs = displayBanlist();
            bannedSongs = bannedSongs.replaceAll(", $", "");
            sendMessage("Currently banned songs are: " + bannedSongs);
        } else if (command.contains("!qhelp")) {
            sendMessage("These are the valid commands for the Beat Saber Queue system.");
            sendMessage("!add <songId>, !queue, !blist, [Mod only] !next, !clearall, !block <songId>, !close");
        }
    }

    private String isOSTSong(String parsedValue) {
        String songName = "";
        switch (parsedValue) {
            case "OST1":
                songName = "$100 Bills";
                break;
            case "OST2":
                songName = "Escape ft. Summer Haze";
                break;
            case "OST3":
                songName = "Legend ft. Backchat";
                break;
            case "OST4":
                songName = "Country Rounds Sqeepo Remix";
                break;
            case "OST5":
                songName = "Commercial Pumping";
                break;
            case "OST6":
                songName = "Breezer";
                break;
            case "OST7":
                songName = "Turn Me On ft. Tiny C";
                break;
            case "OST8":
                songName = "Beat Saber";
                break;
            case "OST9":
                songName = "Lvl Insane";
                break;
            case "OST10":
                songName = "Balearic Pumping";
                break;
        }

        return songName;
    }

    private void sendMessage(String message) {
        BeatBotMain.client.sendMessage("#" + BeatBotMain.CHANNEL, message);
    }

    private void assignBlacklistedSongs(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                // Added this check to avoid printing duplicates on blacklisted song output
                if(!banList.contains(sCurrentLine)) {
                    banList.add(sCurrentLine);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeSongToBlacklistFile(String songID) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(blacklistFilePath, true))) {
            bw.newLine();
            bw.append(songID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createJSON(){
        Map<String, String> map = new HashMap<>();
        map.put("playlistTitle", "QueueBot Playlist");
        map.put("playlistAuthor", "QueueBot");

        // Its disgusting, what can I say?
        map.put("image", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//" +
                "gAgQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3MA/9sAhAADAwMDAwMEBAQEBQUFBQUHBwYGBwcLCAkICQgLEQsMCwsMCxEPEg8ODxIPGxUTExUbHxoZGh8mIiImMC0wPj5UAQMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlT/" +
                "wgARCADAAMADASIAAhEBAxEB/8QAHgAAAQQCAwEAAAAAAAAAAAAAAAUHCAkEBgECAwr/2gAIAQEAAAAAqqAAAAAAAAAAFJb1bzAAAAAAH2vfcOqWHLG+Mko74/XnZXk3OIQAAWu2f1Q52gw4ULY67YyZtwc2FyrOrzqAB7X+6OiuZEKPDsZ0vZKaRtSjCKONewABYXc3Xi1YkRvdy4fD3PqJ0fIDVzgAOZ9HqzHStT1bfXbJJxLHY68RUqHj8ABKa+rw9IZwYR39sv3VETfJSX8L5odIAAsgtG25T07VkFlZjNM0bSr7syl0Kj6MvABn/QA6+xr2sYnlA10Y77astw701l+u+vePwBIq+3rt/RKxcWn+YUc21u4helyr3CkuEPABbjPDLU1Lr11KuC0iBrMzfbXV37ayjvqAc/QlJJC1dY9O3hEtsJjtZIfeNQi7Xcxus6kBz9BbyLikgJ+Kvrsc4lvbI1zUSJ1cOnJbH4wOf9GmLseSaTpLw5lfvD5xQs8g5OZlqCdAxwdWSViUr+Omj6q6qiNnX7LiJz4yLWNXiVDGGoKi/" +
                "efKbwxMFe7Bise3DRSUSGznA2FL0VALf7KEDYADhMykuBuG62pTw4jj8++CGV9Ibs9jjwyDHT1dL0aMEgXa4QGkosbc7WeWxpqt34OQPFLwK3prMY3ssHtrirZaaVV52P74myqWNm8mGg8cRXalQp9crZ9VbrYLv3o1Jy+ePfI65CLnndr47uiybpuIzkHYaJc3IvbXg4Li7JnJDa7RtKahaCvbGntmraOppXkAAG4y3hLggO7vcZ9xlxBYP//" +
                "EABkBAAIDAQAAAAAAAAAAAAAAAAACAwQFAf/aAAgBAhAAAAAAAAAAShbgu1GtAGVmNq2TJ3g72HDTYnjaYJRbWXdarFKxKyvWZtTCmDszkddebcFAbk8a506SzA69VjAsS3+9OA0mIyX7M4i8k7RHJAADgH//xAAbAQABBQEBAAAAAAAAAAAAAAAAAQIDBAUHBv/" +
                "aAAgBAxAAAAAAAAAA0Oj87pwTJEAnaOucr4xE/wBh4kGx+h69zzxqOSMM8m8/7bZymVmIZ0Vipt9a4/5f1zQjzqyWNV69ExvJEMmTbffa9YmpXlSWBnpaUVYayUSKntq5ufkFmeWgzSmJoQRHNc4D/8QALxAAAAcAAQIFAwQBBQAAAAAAAQIDBAUGBwgAERASExQgFSExCRYwMhcYIiQzUP/" +
                "aAAgBAQABCAD/AMCOhpaXP5GDqhXZkTzuFUVUFBTV/iEBD+LAMUltsuiMShnGIZ5m0S3Zw8rHwYsz/UuT9L43WKaB/EvMvpThioeJcIHbLHRP1nfHGyXOjTdxdlaKqLqJJHIKZhKPRSmMPYIam2iwLAlG1rizs1kUAE9L46RmLUYH9r/h/" +
                "TaIHoXQwzUxHQUW6fvbzpGg8k7I5j4WSwcmcRrV27slJg55gdI1qpR406yrbNaj7uYYS845lc+3LLndLo+rYfA5Ix9w2GOf+XzDAQqs9Ms40uI8RswiI9q9eRFJgIZIqbUTxTJVNE3L3HYVawS9qnVAKBzAX5t0FXSyaCPETJBy3L2gPOcNsfxlJh6yy4xNYeCn/" +
                "aH5Hz0W4iWcah1bk0Prz0heM2u17M21qh7LYMr1PXzP7/XcHwzIZGnRdiAucUMCeTqd4649YROZzneY1bLoxaOr9/tSNKqUjNKQNxkoyQJN2TnYyRdPqpNB/Bwx49jZJEt9sTcU/bpiTnEQW81QJMyaqqKhFUVll3Kgqry8m3iGKrhZ04O8dKrn4vUyFuWxO1pSvJp+0OmFUqqVXcS4Nih3HxlYmOm2J2Uhaqg4gJ9s4S50LB6VYSH55BQHWmaDC11Co1qGqFdYQsc7UcFIAN+Q2Zf5Tz1/FoQOhtkEDxc3IaOiUolZSkw/l1fUdP3xGCQD1xPyl5QacvNS8KiKTQBHoOhDw79ctaJL2nP0pqG3zYC61JQqxPnw6lvpe5V4vTRyJ1+wwcAaGWenF+3OmPnLunGWuag5NNRs3g24VlwdA5Mk2t23XVJxIxOCsbAbxYo6NO6OURKUCFApZ+zQNWZleTCGk0Rwommg50GuoH8hYyyxsoPZMBAQ+0k1TexrxuppMEat3mdjDfPglliLyQdXh+6nEoUyBz1ufbT7QyyQgAh2GbjlQH/" +
                "iiVwiYS9XAjs1TmgHhMmg8y8xDSkzHQbfzLSulPFhMVrchj7zGnjbBXciz6sSxZSMe6dn8Q99B/SLBE2hUD19mmok1SKpqFwmqXWVX0PuGXbE4tUtbJ4SmKIgPxi2CspJNGKOO0cmeUODgwcwCNgTImet15Otshb+Es8Sh2D1/IRr5lMR6L5pJNknse7bKcW7QjnExoNNdWTR4VR6P1Oe3ZwaTctK7RNGi70p7RG253rd0sTGqxW78dqPkUlSXyPDEkNVCW+rmUduVx6akUBsX1uX+16hQXv0dro9az+Gr6EvWvjxWqRLltdeYHmmyaDlNUsEV+RZwZwk4+/Yyp/v06QbvW6qDluzax7Urdq5MCbdU47DZH0rs8lL53TuIuZnrKC9josmnR465ZwQky4qOox8owhnhpCIYPDcy59vKRkLQWTfKnsFLNbdEZbcYy/VdOWTnnqUbDvnSuicvX9+r05V7CPbv9vj+n5kTNtDr395Y0DEnCuhbPyAACVnMs3qqiRAOBu4dG6EO4dXQ6gVeWI24q32zVOdTpr9i0jXICq35hZshWXrXRK9jmK3az3RpOXmA3B262B5R4vlZW5qAtVS06Ofcns1TYArG8X6vZK7QFHc7zN3uJodKd1ePOcyhzHN8Q/IdcOCJp4RAAR6xaPCgK0nW0RT9RomJkVPIoKgh2HpJycTdulHA/jpuiaQVFMvIWgXcbXAT9OwW+WXCtDeUnRNtyT/" +
                "AC3BRZo1jxuXsrgklqVGoVLo0cLWtroIOUjpLMc7o8S9M9abxyYXrcmNFoPIXD7PSo6v2G1FobPyfeaqjqLTFZP4B+Q64agdPBYDzzlqeoSjiKjalYP3JCIPjScO3kEzB0tCqsmpO6X+3v3WXAgiI1jzgkqr0+i2z0hhU1rIIHQoo8PMr6ZtPFxIjWVp/MjN740QLJUfQaZPugZRXUq4SaFKd5rXGFeYs4aDQdsuOoWi9sWOjmUTJ280vKR8e3Erp2KAuVBQ8AARHsGUUFK+aBFVt/RKjEUmrR8HF2ajsrE5K7GKi2kKwRZNl102yfqHWOiZAe75xHthETPgUssi2i2UTGIRLIjVFwKwIKeju07yMRRSQGpcUait7eat1OWx1WrzzmPoW7WW9aG5QobNwqnFoLP5WSluRGvpRcc3QTboJop7phFU2atrtX2lVC2Z1bHsBOOHbl2ICt45Flc7rVnCGiLtA1S24pMWbrh/f5K0ZFDKS3gqummAiexT6TNmoqrW6mraDDLzkdDRkSUSMvDXakpcKhIxiTjcrNl+czVIuPFqmJQmHw6D/lBkiNd0GpSVP03WZyzVqGplWyjOYnMKe0iGty0GvUeDdysnjukv9Ogl5hXnzlDWw1JvbmL/AInLsMMLohh8YibloF0DqNgkI+cSlUHnGTNZWm5hAsH/AEqmcxh8osznH7v6w0enIdUhSkKBQ8XqqJkwR62XDq/" +
                "qtSViHPHW6W+El7Pnlrkto2e8XCZeUjEMofVsrm1WbWN9iKXKKwbSr43f9fmm9j0uOYs4pkizZbBGtZ6mz0YvZ94eI4g3ytv8OCGI1wlT/e8sHbt9oYliRkHxn/yeSSAdkUzEcER7pFVeLtyKO9Dm77i+oz1veaJl8TTK9H6fm8fynpzGvNnI4Ei/0TY7FoavRjAQBMO26DB0ekTkrKybsH8i7dB4smx3jxBuTCqh+yMqrUEKSRUi+UvgIgACPRkVDuk3Cfg/RcKtj+jExibdJMx1CedMxejoqIJkbku1cbW+pTUC64f2witNn6LLNsbxk0j70rNGJjWxG7RWUZJdP5gzonppabgdc2SKSTndXziay26SNfk/BMh1TlITivw9eqvYq7XM7tqzQKRNSYXFyl5GzxJUoAHX56AAAPiu6TRL9zySo/gFOynn6cW9HDuUVkaHG4wScD9bUPy/xAhDieb5uU9ZwRhUsum5OQhmr6yg6bmKBg5t0ZheK4pY20pl60bnUfbS9cWMUldLv7F248qZCFTTWOomQRImYRKAisyFV82dlLKn7dNnSLn8yf1QqAGj0xMJCibwePSoB5SmUMobubrt1yHxWbuDpjdKfn+5V2dXGvXrkBmeRIZTMyLZAXzVQqqEPZdDi2q7lNvyO2iMRFueybNodqYHYyJ5F2o0I1GjzcLXrG0fzHGrSYXRagueHstwqtNai5n69teU2t8DKKcrtmSAuHTNwzk0QWZCciq52YJEVblKAe6cj16j8yxTFXlQjW513kBodYtouE4FQFDnERBI/" +
                "XpK9a1qcJjlZCfmofnhjsi4TSXu+X4xusKxsDyMwzjvW5Qq1kKlxED2/ZPUMATblbhotA4o6qwWDqf4WTorLq1ebiHkBLPIx4A9h6zDmnZc5ow1wlz0e2X+ZVkp643+tS8VFEgJvb9OsEChCP6xr+i1DzBFteWG3MxAyaHNXeUgABDnBunT7mZu70olGe2rSrkZJObkNiRo9wCWyr/WpvPbt065hbu6/u45RbS5/vZ9WvtxbC1msmqD+zLyIN5mha/UDM2czqueM6XGREi0qMIe0TzWMNoVTJSp40c3I6cp/wBG9ksLT/oXcLuljrL/AMNBpUzoVqZ12Iis+0XF6csSSkiuQfOPcfDLNR0TM1nshV9A5U6nplccwM11R16u0ssOvYdonOJ1sSYhUPH/xAA+EAACAQMCBAMGBAIIBwEAAAABAgMABBEFEgYhMVETIkEQFCAyYXEwQlKBIzMVJFRicpGUogc0QFCCk7Gy/9oACAEBAAk/AP8AsFjc3TZ6Qxs5/wBtcPapGvdrWQCkZHHVWGCP+lJisoiHvLj0RK0e3V4lGblkBlc9yaitjCB5vFChcDvmuK7DSdSXKXFtDExRyP8ABXGtlLdICRbvHIpk+xIr5kYg+yQafpthAXiaUEGY9hQ3FC3+S+tYyOxz7ASewrSrq4c9AsZrh2eBP1yVq0c2u3pxZ2MBwUH6n/Cg9bbElTpDa2sTSSyMcAAVLd2PCsMxjiig5SXePU1pMKrOPnYl3/" +
                "8ALdVnEkgBKOo2nP7UCRGx3oeorRtUueH4pc3EtrDvztPStZi0vFusXuzERyxqvcVxvbXmrLJsFray/wAVc/araXHfaamit2uZQglmO1Fz6k0bbiK+KBnlBzCh+hFWNtCFGAEiWnhilf5EyATXH0ayshaz0uRxvUfpRaOVB5fgI0kkjBUVRkkn0AqPGo6kBPcfZuaCpSj8Q33u82OoRfNTQwxxWJWIPgZY1cxy3G/cyqc49nNGbzCjFJYKFls4HjDl3bnsUVoo4XtooQ1va25EElzHnJJC4rSTe31whM73jGZxIpwch64c0vGP7MlcOQIWHWACI/7aS4WCaTewmmMpB+hahkwRkRL3kb5aTUrzU9WAli8JHaKCNzyUAcgRQcS3Fgwlz+DasdM09w9nCRzmkHRqXYpQEDsO1c7ddTZD2B2VI0bgAq6nBFSvLIersck0wGFO0dzR80jEmrCK8ttOgD7ZFyofHI0ihANgUDkF6YratteXIliiHSPlzA+C3S4t3ILRuMjI6GtTENmwCR2zMAh+iiuvgE/gZAu7lRI/ZRzNW6RW9nAkaIBy5DnSKwzhs0ANQtcz2L9pBUMttq1gTDcwMMHK1bkt3epS3ZfQUC8znbFGOZdj0AqLZqutkSOp6pH1SurnPxXMsOo8OTi8hWMnMncVA8H9HWPu8iN6uD+AM+8SlKPWruScXL7sOSdvrgZoVKNK1xR/PQeWX7itGi1GJfkmjkHmrhsWyRRs7ySSAYAom9u4LrZb27fIhWl2RLjHLAwPQUMADkKvobOBpAgklcIMn6muIdNneQZVIrhHY/" +
                "YA1MXP2qQBux9iB1ltpVKnocqaGDb3sox9zn8CPK2rGKyyOkoqJppJ5RHHGpwWY1G0bpIyPG3VSpwfZtDbuYPagwP0FBudlL/+TWD4V7KG++80yjA8qCkEadz1pFvLVm3eE/PB7irSVZ4gdhdhtUGtftYZQ2GXm2D+1ahHehGAYxflo5YKM1odxrN452RW0JAOT3zXCN1YJqEvjkKAVQY+lAgg8/i5yXM6RJ93OBSgSpapJcfWQjnUrQyQuJIZV5lHHQ1O1xIzs7ysACxY+yYmCLzDHVVFMJIJVyjUMrLBIh/dSKkPvFrqTS2sJ/QRWtWkMpPKJ5MYrRpdVgtWKz3KAmPI6hSKglt9QHW1cYY/YVbT6VpdwN19qhBGE6lRUD3FleXDWuoSSElnfZydqtEj1nT70EuSS0sTruDUx+woAn6itNjg0K/XYNVCbyAa4qseIRq0MQus20kNzbXYYu6hZAuFwObY+JQVjc3H/p81EAuoG2pFKFh4IAxhfagkikUq6noQaiEUKfKg6CsAKpJP0FWN1KdpsdSvIYi6CR/Wjf6prN/bCaW4a4dAryjIAFWCJLba3cSLO2DIYScDnXKWG7jJH6voaGJLi2jkYeg3rmrMXWt6zdp7sengYO7fWotBxDDZKl0MZS6wBlSKiEcqyyQTJ6rLCdrUwVILeSTPT5VJFaFBd28kkq2k2QGgIOA1dPiizczu0Nkf0AcnqSTHgqoTPlo+YU2JIsbx96PtyZjaS7cdclTXDqW1nq91LNcXc6eeVg5AIq4SWNGIBRgwBHpyq4jt9TcLbS2XpefYd6sk0PTYJEnPvUiKZz1AWtJN/" +
                "otpbxAalb42QHb8rmrOS9i0aYC+gjGSsOMZqW4v7+RP4VjHC+/xD+U5FQPa3WqXtxdC2PWNJX3LVykms6rFsKIcmFO5o5LEk/c/H3lqMN2ryMnpS7HB5+maNc6NAbB87GoIJ7mwjNu0L+RcOcl/LUuy11+Y3FldlyYUkkO8oGNakbS/0u497sZlwV3ketcQT6zPGmyC0V/DghUdimK0+K1iY+fBLFvuxqNJEYYZWAIP7GtEso7hjlpBEtWL6rxRcgIBEMpAG9a1qW+17iC6b3qNjkRZXNXEm7HYUfFi9T6j4uu+arVJriG2E7Fn2ALUZiZgwdc5AIOOtDa+OTipvFIGCe/twVZjjBpdpxyIqE7wd1rdpykhYdCCKv7HiTh6A4hD3CJc4qz1HS5n9RbvLHn/AB1fSXMrgn+WQB7L6K0hLhQXYKCT6ZrVf6O4kSMMn50nqE2dzpcQjjTZiOVxyLg06jPTJpsiQEBeuc1/LLEr8Fz7hHczoszyeQgH71j3W0hVFIPXA61cT2k6qFd4W2ll7Gl2xxLgCumQM10I9auEA+pqZljkybiVOqqPTP1ouY0HVjkmgPE2nZnpmvcdL097oxS6lEhUxpI2FNa7e8TXDxCXfLJugYdc4atHsDpWgblm2Q5Q7Bk4qz4c4d0y1uRGouoCJbhc08YkWLdKV5LkDnj6VPInCvDVyHuZkOFuJ09KGEjUKo+gqBItQjQ+6XijDo1eKk9pIVRj0ZfQipGcgYGT8E1vFNHGZiZm2jCULfSeMv8Ah9NYWd29qQg1CK5nEKB+7p1V6naadXli8VupCHA9uDjoKbbHGOYpGEL/" +
                "APLWpJG1e7VAsSn9z7ZfCeeBlRv0vjy1YzW+sWVn7tpF8oOy6QnaMGoQ02pQvLdo35i7VwNdvawXRnv5LAOTMVIYA1Yz2eva5DGs0DAh7OI9S1KPECB7qY/NLIeZJq6SKGBCQSfmPYVpk1jatOy2wkBUyR+j1APfdN5TEDrDWtRvMVVzbDBTY3wXk9pMARvicocH7VJqc+q3CRNpyQASJJP4nmEwOW+UnaRURglwZ3U9R4nP2GjWCvLKEdcUAABgD4F3mTkaeOC65NaXRTc0Lip0uG4ZwsF6qBFaELmp7NtN06Y20VpJt3O2dpepFu+ItUJeWTqIg3PYlW9xqes9I7GBCf8AMipja6ZE4ktdGQ8j234qBIIIUCxogwABSgxy6bMpFBy1tqdz71OT1RZPIB8NlFdXt1JmyaRciJB7JYXtmcm3CAggem745AZGbbgUBlUJ+5qPY4bGPpWiJf8ACGuTql3Oi+eFCMGrq8NvLNHcXdsr7kaOY5bkK1WEsYRmDY28HtWmNHp0kLRW8sqdWPQr7OgFTqglt3it4yecjkcgKXaJ53kx23HPwKWaWVEAAyTuOKj2PDZgv9381dM5+C4YRgfyx09rlWx6VHmY5LMaOMgjNAuh6ueoqMPHfWckWCM4JFKZ30C/ngKSjJMTvgVw5ZrOzbtxz1oQwQoMBEAUVJn7UCqGnljaCMi3C+h7moWQwSkwt6PGT5T7VLMxAAHUk1iG2TE9pYnq/YtRGAoAx2FRjwhnec0QDnmPwOvahgV3qylubHW4IZXSFSWBxnOBUrx2wgEzAoQ6qe61q12hQkFWt2FaVJqt3N5YzKTAoNXdi19cgSG3hlRvBB/" +
                "L5etN5cUsaXmjnG/oZENavpsq3MYM1lvb3mJjMY1AA5EEYPstHXSNOlWa4mdcKSnNRShI0UKiDoAKQuw6LQwSBkVPLGYD8inCv96FciBWwyg9HUnlXJsDPtOWNHJ9swt+JdJBIX+0p+g1bHRNajbwbi3nGElYdqg0q1vViaW2mTG92xyAxQlR/wArKCDV9xBA+0e7vDvC5/vVxDfkYwRKzZrWJ3t3+eMMQGqQ+Ev5f3zWkQ6raROC9rKSFf8AyrhcaBa2bAIiJiOTPZq1e00+P08aQKTXFFhNcnpH4oy1TxwQDrK7ALVzFcxHo8bBhW8S7Ccj0FMxI9ac1KwQDmvep0ghQZaSRtqgVrFnqL2xIlEEgYpXU0KWra5mtzMIgsChjk1BqdruPztGMCpLZA6B4dQiYK9cfR6zFAf4Nld3ICJUvDX9XbMX8bpWvcNiJRgJmOtc0Kwv3+S8gmAcVxXoOqWiAmPNziVqULcWkzRSgfqX2aLbTvEhFtcLiPZWoz3TSybjGWOwDPQLVhcaPfWMcatLFHGnjHnuJdGyMcsVr909nCAFUOQxx3atfvYw35WlZlrXxnuYgTWs27/e3WtStf8ATrWtwoP7sCitev5LXxFMqQuUyuedXOq6bZNaRRFNQMbzMyDzmQREpzNazb/6da19B9oAK4il/YYrWbm7hznw3clc1we/EX9XaNB4s0awO4wJP4XUj0Bo6xpGjyTYVhJIYoV+vQVx/ovERv1US29ndSTTwvsDlpAyIFWr6O0WdsGaVgqoO5JrVYtSiCBhcxEFDn7VK6/Y4rVb2L/BO6//" +
                "AA1I8sjnLu5LMT3JP4SRPe3Udy8ayMVUi2hedx99qHA9TXBmkaqdatElia5ti81sJFyAc4IfnzHoag93lMjFotpTae2D0+HUGgiRUN1GXAjbJ2rlfWprRrOcYcJHg+xPG0+PUYTeRFXIMA5t8nOrqXRha6XqDPFbaVcD3m8wPdlcy9F+D//EACsRAAICAQMBCAICAwAAAAAAAAECAxEABBIhMRATICJBUWGBBUIUIzJAcf/aAAgBAgEBPwD/AEJJEiXcx4xvySxvtaFvvrkeojlj3qfrIdVvdlew18LWWKz+UzkiKMkD9jwMQzO3nYCvRR4tfK0U8JqwvNZqJjPIXIq80zAd6Gvbss/WQASwqW+hiKE6ADGB2nbwaNZoe9TWFHuyDfgAs4RtOarRjWRijTDph/F63dWz7vNm0mBPMxPnORJsRUHoM2n2xicRVJDEcgUD4Iltv+DJav5xGIOd5fpkGm2vISOrnnECEsFolevxlqXVBQsgXkyNuIPBBIOAlTQW8VtwvthsvjoSbrAKxeuO5Ln361mgUQ6uaaRxsaPaFyMIZl3GlLC/gZ+WGj8jQkFz/lWd0jNuI5HQ+AXfGLKRw2DaRxjDYl4ESUk/vWSh0U0PN6A4hcqCwo+owOWYjbn4/TRSRM0ig80M10EULju2HPVb6dpBWs6nFAQWxx5C5+MS7oGiRxkUMxlZpiSVPF5NPHCOTyeg9ScUrCpeVgGbk4moHdhgSFYjCS3JN9rMWr4GBiMLFqv07Kqiceytnk4JO41BOpJNcp6jnIIBqLmksgk7VOa1v61RerMKGKKA7KJwqR2jCS5xQR1zVTPDNTKrL+tjO/" +
                "1TClhrIDvlMkkgLDgDItRFKLU3TbcQAnLA7CgOd2c2HAoA7NVo4dUATww6HNNpmgjKs92ceGM9VU/WJHHGKRQo+BgJGWcs5Z983H3yzm4++WfD/8QALhEAAgEDAgUDAwQDAQAAAAAAAQIDAAQRBRIGEyExQRBRYQcgIhQyUnFAQlNi/9oACAEDAQE/AP8AA0zS73WLtba0j3uQSSThVUd2YnsBVv8ASe5v9PFza61bSP1wI1LISO43Vqmj32j3RtrpNreCOzD3FPDtAI6jHU+nKVcb2x8CmCAdFPXyftyK+m2i2+s8N67CJBHNcssJkAyVXGa4c0SLhnRorITGQRlneQjGS3U19REimsYpwAHF1hD8MDkVKSshAokk9SaiMYlQyAlAw3AdyK1w2tzpUcsAXaGXbjwO2PsdtilsZwKSXmrlRXCXGl3wZfyNt328+BKn9diPkVL9UOHLuEO9+QCM8vYQa4h4ibW5Y2CmO3iyY0Pck/7GmbcxPvSxu/7VJraR3GKLuEMYY7CQSPs1GYxQgDu7AVY8zb/561dxJLHg0bEJ+W/5FQR/qUjfOE2jFR6dJyOcLd+V/wBNp2nHzV1epaI/Kj3bASQPOKjY36JIu0IQCD/dMuGIz2NEY9dTKrbZIz+QxVpcIEChqZ91Tj8CfjpVmsKW6o7sVQYGK0XizQYeE10+dTHOjvvGCRICMA1dM4hmaFQX2sUU+/gVwu+rhZ47oSLEoGwMMYPsKDEDH2SbNuHGQfGM1PYJIBJBhSR28Gm5sTgOCtRNz7hYz2800s+nEAgG33Ht3GahlinAZWyp8imwGODkVgAA5rg/RNOvbGae7ijlJk2rluwFcWaRp+mXSfpJV/" +
                "L98O7JQ+qvFcBh32tgj2Io4RfAAqeRruXlwoDt7satLQW65bBc9zV50h3FQwVgSD7VzITEvJ27WHcUkbOenbyabMhCoOg7UDNHujV2HkgHocU7vIxZ2LMe5JyfQjIqGBbctjJMjEsaaJWPWo4IoixRQCxyfQyCTcqEHHfzUOyOTYqhVPikto7u2H6dlRx0cOwGRUzmAmJSOnQsKgA3lj2Ao+hdVGSRSTRucKfVhuUj3FIkdvGFGAAKnkRmBU9qgUSRAgkHHXFcuEd5M1J0QIinBq5s57UqJFwWQMP6NTuyRkiirsCeppc+KS6kU/l1FC8TPY0LmMjPWpZmlb48CgCxAAq2up7X8WUlamu1kcYQ1HO+em5almlmbdI7OfcnNMquMEVsXp8VtXGMCuXH/EVyo/4Cti+1cqP+AoIo7Afb/9k=");

        JSONObject object = new JSONObject(map);
        JSONArray arr =  new JSONArray();
        object.put("songs", arr);

        try (FileWriter  fw = new FileWriter(playlistFilePath)) {
            fw.write(object.toString(4));
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(BeatBotMain.DEBUG){
            System.out.println("Playlist File Created");
            System.out.println();
        }
    }

    private void processExistingJSON(){
        try {
            //Read from file
            File file = new File(playlistFilePath);
            JSONObject root = new JSONObject(FileUtils.readFileToString(file, "utf-8"));
            JSONArray songRoot = root.getJSONArray("songs");

            for (int i = 0; i < songRoot.length(); i++) {
                String song = songRoot.getJSONObject(i).getString("songName");
                String requestedBy = songRoot.getJSONObject(i).getString("requestedBy");
                queueList.add(new StringPair(song, requestedBy));

                if(BeatBotMain.DEBUG){
                    System.out.println("Adding \"" + song + "\" to queue, requested by " + requestedBy);
                }
            }
            if(BeatBotMain.DEBUG){
                System.out.println();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void removeValueFromJSON(boolean notNext, String removedSong){
        try {
            //Read from file
            File file = new File(playlistFilePath);
            JSONObject root = new JSONObject(FileUtils.readFileToString(file, "utf-8"));
            JSONArray songRoot = root.getJSONArray("songs");

            if(!notNext) {
                if(BeatBotMain.DEBUG){
                    System.out.println("Removing:");
                    System.out.println(songRoot.getJSONObject(0).toString(4));
                    System.out.println();
                }

                songRoot.remove(0);

                for (int i = 0; i < songRoot.length(); i++) {
                    int currPost = songRoot.getJSONObject(i).getInt("position");
                    songRoot.getJSONObject(i).put("position", currPost - 1);
                }

            } else {
                for (int i = 0; i < songRoot.length(); i++) {
                    if(songRoot.getJSONObject(i).getString("songName").equalsIgnoreCase(removedSong)){

                        if(BeatBotMain.DEBUG){
                            System.out.println("Removing:");
                            System.out.println(songRoot.getJSONObject(i).toString(4));
                            System.out.println();
                        }

                        songRoot.remove(i);

                        for (int j = 0; j < songRoot.length(); j++) {
                            songRoot.getJSONObject(j).put("position", j + 1);
                        }
                    }
                }
            }

            try (FileWriter fw = new FileWriter(file)) {
                fw.write(root.toString(4));
                fw.flush();
            }

        } catch (IOException e){
            e.printStackTrace();
        }
    }
    private void addSongToJson(String songId, String songName, String requestedBy){
        JSONObject songObj = new JSONObject();
        try {
            //Read from file
            File file = new File(playlistFilePath);
            JSONObject root = new JSONObject(FileUtils.readFileToString(file, "utf-8"));
            JSONArray songRoot = root.getJSONArray("songs");

            songObj.put("position", songRoot.length() + 1);
            songObj.put("id", songId);
            songObj.put("songName", songName);
            songObj.put("requestedBy",requestedBy);

            songRoot.put(songObj);

            if(BeatBotMain.DEBUG){
                System.out.println("Adding to JSON:");
                System.out.println(songObj.toString(4));
                System.out.println();
            }

            try (FileWriter fw = new FileWriter(file))
            {
                fw.write(root.toString(4));
                fw.flush();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private String[] getSongFromBeatSaver(String songID) {
        String songName;
        String uploader = "Jaroslav Beck";
        try {
            songName = isOSTSong(songID);
            if (songName.isEmpty()) {
                JSONArray json = new JSONArray(JSONParser.readUrl("https://beatsaver.com/api.php?mode=details&id=" + songID));
                songName = json.getJSONObject(0).getString("songName");
                uploader = json.getJSONObject(0).getString("uploader");
            }

            // Haven't looked for a solution for this, I'm sure there's a library to help unescape these characters.
            // I've just been adding them as I encounter them
            if (songName.contains("&excl;")) {
                songName = songName.replace("&excl;", "!");
            }

            if (songName.contains("&lpar;")) {
                songName = songName.replace("&lpar;", "(");
            }

            if (songName.contains("&rpar;")) {
                songName = songName.replace("&rpar;", ")");
            }

            if (songName.contains("&period;")) {
                songName = songName.replace("&period;", ".");
            }

            if (songName.contains("&lbrack;")) {
                songName = songName.replace("&lbrack;", "[");
            }

            if (songName.contains("&rsqb;")) {
                songName = songName.replace("&rsqb;", "]");
            }

            if (songName.contains("&sol;")) {
                songName = songName.replace("&sol;", "/");
            }

            if(songName.contains("&apos;")) {
                songName = songName.replace("&apos;", "'");
            }

            return new String[]{songName, uploader};

        } catch (Exception e) {
            sendMessage("Song with ID " + songID + " is Invalid!");
        }

        return null;
    }

    private void addSongToQueue(String songID, String requestUser) {
        String[] beatSaverValues = getSongFromBeatSaver(songID);
        if (beatSaverValues != null) {
            String songName = beatSaverValues[0];
            String uploader = beatSaverValues[1];

            boolean existsInList = false;
            boolean limitReached = false;

            // Checking to see if the user has requests that equal the Viewer Request limit
            if(queueList.size() != 0){
                int internalCounter = 0;

                for(StringPair pair : queueList){
                    existsInList = pair.getFirst().contains(songName);
                    if(existsInList){
                        break;
                    }
                }

                for(StringPair pair : queueList) {
                    if (pair.getSecond().equalsIgnoreCase(requestUser)) {
                        internalCounter++;
                    }
                }

                // Override by default is on for Broadcaster and Moderators.
                // If enabled in the properties file, It can also be overridden for subscribers
                if(!isBroadcaster) {
                    if(!isModerator) {
                        if (internalCounter == BeatBotMain.VIEWER_REQ) {
                            limitReached = !BeatBotMain.OVERRIDE_SUB_REQ || !isSubscriber;
                        }
                    }
                }

                // Doing this to avoid a ConcurrentModificationException
                // Lists are something else...
                if(!limitReached) {
                    if (!existsInList) {
                        queueList.add(new StringPair(songName, requestUser));
                        addSongToJson(songID, songName, requestUser);
                        sendMessage(requestUser + " added \"" + songName + "\", uploaded by " + uploader + " to queue!");
                    } else
                        sendMessage("\"" + songName + "\" already exists in the queue");
                } else {
                    sendMessage(requestUser + ", you've reached the request limit.");
                }
            } else {
                queueList.add(new StringPair(songName, requestUser));
                addSongToJson(songID, songName, requestUser);
                sendMessage(requestUser + " added \"" + songName + "\", uploaded by " + uploader + " to queue!");
            }
        }
    }

    private void addSongToBlackList(String songID) {
        if (banList.contains(songID)) {
            sendMessage("Song already on banlist");
        } else {
            String[] beatSaverValues = getSongFromBeatSaver(songID);
            if (beatSaverValues != null) {
                String songName = beatSaverValues[0];
                String uploader = beatSaverValues[1];

                banList.add(songID);
                sendMessage("Added \"" + songName + "\", uploaded by " + uploader + " to banlist!");

                writeSongToBlacklistFile(songID);
            }
        }
    }

    private String displayBanlist() {
        StringBuilder bannedSongs = new StringBuilder();
        for (String id : banList) {
            String[] songs = getSongFromBeatSaver(id);
            if (songs != null) {
                bannedSongs.append(songs[0]).append(", ");
            }
        }

        return bannedSongs.toString();
    }
}