# BeatSaberBot

Get your Oauth Token here: https://twitchapps.com/tmi/  

Available Commands:

[Everyone]  
!qhelp - Shows all available commands  
!add <songID> - Add a song to queue using the BeatSaver ID. The Original soundtracks are stored using the values OST1-10 in the order they are displayed in game, this will also create a .json file for playlist or continuing the queue in the future.  
!queue - Shows the current queue  
!blist - Shows currently Banned songs from the channel.  

[Moderator Only]  
!next - Removes the current song from queue and displays the next song. Also removes the song from the .json file.  
!clearall - Clears the queue  
!block <songId> - Adds the song to the banlist (File and array)  
!close - Closes the queue.  
!open - Opens the queue.

To use the bot, place all files in the same location or in the same folder.  
You'll be using the following files:  

BeatQueueBot-2.1-SNAPSHOT-jar-with-dependencies.jar  
run.bat  
song_blacklist.txt (Auto-generates if it doesn't exist)  
botsaber.properties  

Edit botsaber.properties to have your channel and Oauth token of yourself or a bot you have setup for your channel.  
There are new options here too:  
oauth_token= # Place your OAuth Token here  
twitch_channel=mr_moogles # Place the channel you want to join here  
moderator_only=no # This allows only Moderators to add to the queue as well as perform any commands  
subscriber_only=no # This allows Moderators & Subscribers only toe add to the queue  
viewer_request_count_allowed=1 #Default: 1, but this is the number of requests a user can have. Moderators don't have a limit.  
override_request_limit_subscriber=yes # This removes the request limit for Subscribers  
output_irc_output=yes # Default: yes, This will output all of chat to Command Prompt/Console Window  
debug_output=no # This helps see whats being removed, added, who has which permissions and prints it to Command Prompt/Console Window  
continue_queue=yes # Default: yes, This will process the JSON  

Example blacklist file is just song ids on new line.  

2021  
2029  
